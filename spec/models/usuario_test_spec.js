var mongoose = require('mongoose')
var Bicicleta = require('../../models/bicicleta')
var Reserva = require('../../models/reserva');
var Usuario = require('../../models/usuario');
// var {Usuario, Reserva} = require('../../models/usuario')

 //remeber beforeEach afterEach are call before and after of Test

describe('Testing Usuarios', function () {

  beforeEach(function (done) {
    var mongoDB = 'mongodb+srv://socratesvallejos:Jameslebron23@cluster0.z0njd.mongodb.net/testdb?retryWrites=true&w=majority'
    mongoose.connect(mongoDB, {
      useNewUrlParser: true
    })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'connection error'))
    db.once('open', function () {
      console.log('We are connected to test database')
      done()
    })
  })

 //Note here we can use Promises
//cascada to delete all dependencies
  afterEach(function (done) {
                          //callback
    Reserva.deleteMany({}, function (err, success) {
      if (err) console.log(err)
      Usuario.deleteMany({}, function (err, success) {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, (err, success) => {
          if (err) console.log(err);
          done()
        })
      })
    })
  })

  //Test
  describe('Cuando un Usuario reserva una bici', () => {
    it('debe existir la reserva', (done) => {
      //Create a user
      const usuario = new Usuario({
        nombre: 'Lenin',
      })
      usuario.save()

      //Create a bicycle
      const bicicleta = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "deportiva"
      })
      bicicleta.save();
  
      var hoy = new Date();
      var mañana = new Date();
      mañana.setDate(hoy.getDate() + 1)
      
      usuario.reservar(bicicleta.id, hoy, mañana, (err, reserva) => {
 

        Reserva.find({}).populate('bicicleta').populate('usuario').exec((err, reservas) => {
          console.log(reservas[0]);
          
          expect(reservas.length).toBe(1)
          expect(reservas[0].diasDeReserva()).toBe(2)
          expect(reservas[0].bicicleta.code).toBe(1)
          expect(reservas[0].usuario.nombre).toBe(usuario.nombre)
          done()

        })
      })
    })
  })
})
