var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www')



describe('Bicicleta API', ()=>{
    describe ('GET BICICLETAs /', () =>  {
        it('Status 200', ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a= new Bicicleta(1,'rojo', 'urnano', [-0.2878352,-78.5516807]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response,body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe ('POST BICICLETAs /create', () =>  {
        it('Status 200', (done)=>{
            var headers = {'content-type':'application/json'};
            var aBici= '{ "id" : 10, "color":"rojo", "modelo":"urbana", "lat":-34,"lng":-54 }';
            Bicicleta.add(aBici);

            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create', 
                body: aBici
            },function(error, response,body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
    });

});
